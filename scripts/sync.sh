#!/bin/bash

####################################################################
# This sync script is used to update the `src` directory with the
# latest theme files pulled down into the `dist` folder.
#
# Assumes you used theme kit to download lastest live theme into `dist`

IMG_EXTENSIONS=(jpg jpeg gif png)
FONT_EXTENSIONS=(eot svg ttf otf woff)

# Only run this at the root path of the Shopify project
if [ ! -d .git ]; then
  printf "Need to run this at the root path of the project"
  exit 1
fi

# Sync locales
if [ -d ./dist/locales ]; then
    rsync -av ./dist/locales/ ./src/locales/
fi

# Sync layouts
if [ -d ./dist/layout ]; then
    rsync -av ./dist/layout/  ./src/layout/
fi

# Sync templates
if [ -d ./dist/templates ]; then
    rsync -av ./dist/templates/ ./src/templates/
fi

# Sync sections
if [ -d ./dist/sections ]; then
    rsync -av ./dist/sections/ ./src/sections/
fi

# TODO: Determine value and risk of syncing settings
# if [ -f ./dist/config/settings_schema.json ];then
#     rsync -av ./dist/config/settings_schema.json  ./src/config/
# fi

# if [ -f ./dist/config/settings_data.json ]; then
#     rsync -av ./dist/config/settings_data.json  ./src/config/
# fi

# Sync images
for ext in ${IMG_EXTENSIONS[@]}; do
    for image in ./dist/assets/*.${ext}; do
        [ -f "$image" ] || break
        rsync -av ${image} ./src/assets/images/
    done
done

# Sync fonts
for font in ./dist/assets/*.woff; do
    [ -f "$font" ] || break
    filename="${font%.*}"
    for ext in ${FONT_EXTENSIONS[@]}; do
        filename_ext="${filename}.${ext}"
        [ -f "$filename_ext" ] || continue
        rsync -av ${filename_ext} ./src/assets/fonts/
    done
done

# Sync svgs
for vector in ./dist/assets/*.svg; do
    [ -f "$vector" ] || break
    rsync -av ${vector} ./src/assets/svg/
done

# Sync any found scripts
find ./dist/assets -type f \( -name "*.js" -or -name "*.js.liquid" \) -exec rsync -av {} "./src/assets/" \;

# Sync any found styles
find ./dist/assets -type f \( -name "*.css" -or -name "*.css.liquid" -or -name "*.scss.liquid" -or -name "*.scss" \) -exec rsync -av {} "./src/assets/" \;

# Sync snippets into relative module folders if exist or snippets folder if not
if [ -d ./dist/snippets ]; then
    for snippet in ./dist/snippets/*; do
        basename="$(basename ${snippet})"
        module_dir="./src/modules/${basename%.*}"
        if [ -d "${module_dir}" ]; then
            rsync -av ${snippet} ${module_dir}
        else
            rsync -av ${snippet} "./src/snippets/"
        fi
    done
fi
