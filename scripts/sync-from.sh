#!/bin/bash

SCRIPT_PATH="`dirname \"$0\"`"

# Terminal colors
source $SCRIPT_PATH/colors.sh

# handle arguments
for i in "$@"; do
case $i in
    --shopify)
    REPO="barrel-shopify"
    BASE_PATH=".$REPO"
    ASSET_SRC="src/assets"
    ASSET_TARGET="src"
    shift # past argument=value
    ;;
    --base|--modules)
    REPO="base-modules"
    BASE_PATH=".$REPO"
    ASSET_SRC="src"
    ASSET_TARGET="src/assets"
    shift # past argument=value
    ;;
    *)
	echo "Unknown option: ${i#*=}"
          # unknown option
    ;;
esac
done
if [ -z "$BASE_PATH" ]; then
  echo "${YELLOW}Must use either the --shopify or --modules flags${DEFAULT}"
  exit 1
else
  echo "Using: $BASE_PATH"
fi

if [ -d "$BASE_PATH" ]; then
    echo "Fetching latest from repo...";
    cd ./$BASE_PATH
    git fetch origin && git pull origin master && cd ../
else
    echo "Cloning repo...";
    git clone git@gitlab.com:barrel/$REPO.git $BASE_PATH
fi

# copy scripts
read -r -p "${BLUE}Copy automation scripts? [y/N] ${DEFAULT}" response
case "$response" in
    [yY][eE][sS]|[yY])
    echo "Copying scripts..."
    COPY_PATH="scripts"
    cp -r ./$BASE_PATH/$COPY_PATH/ ./$COPY_PATH/
    ;;
    *)
    echo "Skipping copy of scripts..."
    ;;
esac

# copy core templates, sections, modules, locales, and layouts
read -r -p "${BLUE}Copy templates, sections, modules, locales, and layouts? [y/N] ${DEFAULT}" response
case "$response" in
    [yY][eE][sS]|[yY])
    echo "Copying template files..."
    COPY_PATHS=("templates" "sections" "modules" "locales" "layout")
    for COPY_PATH in "${COPY_PATHS[@]}"; do
        cp -r ./$BASE_PATH/src/$COPY_PATH/ ./src/$COPY_PATH/
    done
    ;;
    *)
    echo "Skipping copy of template files..."
    ;;
esac

# copy config
read -r -p "${BLUE}Copy config files? [y/N] ${DEFAULT}" response
case "$response" in
    [yY][eE][sS]|[yY])
    echo "Copying config files..."
    cp -r ./$BASE_PATH/postcss-tasks/ ./postcss-tasks/
    COPY_PATHS=(".babelrc" ".browserslistrc" ".editorconfig" ".eslintrc.js" ".gitignore" ".gitlab-ci.yml")
    COPY_PATHS+=(".liquidrc.js" ".stylelintrc" "postcss.config.js" "webpack.config.js")
    # COPY_PATHS+=("package.json")
    for COPY_PATH in "${COPY_PATHS[@]}"; do
        cp ./$BASE_PATH/$COPY_PATH ./
    done
    ;;
    *)
    echo "Skipping copy of config files..."
    ;;
esac

# copy theme files
read -r -p "${BLUE}Copy theme CSS and JS? [y/N] ${DEFAULT}" response
case "$response" in
    [yY][eE][sS]|[yY])
    echo "Copying theme CSS and JS..."
    cp -r ./$BASE_PATH/postcss-tasks/ ./postcss-tasks/
    COPY_PATHS=("css" "js")
    for COPY_PATH in "${COPY_PATHS[@]}"; do
        cp -r ./$BASE_PATH/$ASSET_SRC/$COPY_PATH/ ./$ASSET_TARGET/$COPY_PATH/
    done
    ;;
    *)
    echo "Skipping copy of theme CSS and JS..."
    ;;
esac

# remove repo
rm -rf $BASE_PATH

# remove mock json files
if [ "$REPO" == "base-modules" ]; then
    find ./src -name "*.json" -type f -not -path "*locales*/*" -not -path "*config*/*" -delete
fi