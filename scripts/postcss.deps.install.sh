#!/bin/bash

npm i -D postcss \
  postcss-cli \
  postcss-loader \
  postcss-scss \
  postcss-mixins \
  postcss-inline-svg \
  postcss-color-function \
  postcss-import \
  postcss-strip-units \
  postcss-automath \
  postcss-hexrgba \
  postcss-extend \
  postcss-each \
  postcss-fontpath \
  precss \
  autoprefixer
