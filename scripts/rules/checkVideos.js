module.exports = {
  id: 'VIDEOS',
  regex: /<video/,
  filter (fileName) {
    return true
  },
  errors (lines) {
    return lines.filter(({file}) => {
      return !~file.indexOf('video')
    })
  },
  message () {
    return 'Video rendered without video module'
  },
  neededMatches: 1
}
