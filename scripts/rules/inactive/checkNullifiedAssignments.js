module.exports = {
  id: 'NIL',
  regex: /{%-? ?(?:assign|capture) +(.[^ ]+) += +([a-z_0-9]+)/,
  filter (fileName) {
    return (
      !~fileName.indexOf('section') &&
      !~fileName.indexOf('template') &&
      !~fileName.indexOf('theme')
    )
  },
  errors (lines) {
    const nuls = lines.reduce((obj, line) => {
      if (line.matches[2] !== 'nil') {
        return obj
      }
      obj[line.matches[1]] = line
      return obj
    }, {})

    const errors = lines.filter(({matches}) => {
      return typeof nuls[matches[1]] === 'undefined'
    })

    if (errors.length) {
      return errors
    } else {
      return []
    }
  },
  message ({matches}) {
    return `Missing: "{%- assign ${matches[1]} = nil -%}"`
  },
  neededMatches: 3
}
