const OFF = 0
const ON = 1

module.exports = {
  'files': 'src/**/*.liquid',
  'rules': {
    'CAMELCASE': ON,
    'IMAGES': ON,
    'NIL': ON,
    'TRANSLATIONS': ON,
    'VIDEOS': ON
  }
}
