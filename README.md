# Basic a.k.a. Base Modules
This project is a collection of base modules that can be re-used from project to project and ported to other projects. The initial intention is to mirror as close to Shopify as possible using the Assemble static site generator.

## Table of Contents
1. [Getting Started](#getting-started)
   - [Features](#features)
   - [Requirements](#requirements)
   - [Initial Setup](#initial-setup)
1. [Development and Project Features](#development-and-project-features)
   - [Browsersync](#browsersync)
   - [Environment Variables](#environment-variables)
   - [Other Scripts](#other-scripts)
   - [GitLab & GitLab CI](#gitlab-gitlab-ci)
   - [Netlify](#netlify)
1. [Assemble & Shopify](#assemble-shopify)
   - [Modules](#modules)
   - [Sections](#sections)
   - [The Module API](#the-module-api)
   - [Project Structure](#project-structure)

## Getting Started
### Features

* [PostCSS](https://github.com/postcss/postcss) for transforming styles with JS plugins, which lint your CSS, support variables and mixins, transpile future CSS syntax, inline images, and more
* [ES6 JS Modules](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import) for modular development of markup to css and js files
* [Webpack](https://webpack.github.io/) for _'piling_, preprocessing, and concatenating/minifying files
* [VueJS](https://vuejs.org/) for building user interfaces in an incrementally adoptable way
* [Browsersync](http://www.browsersync.io/) for synchronized browser testing
* [The Module API](#the-module-api) API for modular WordPress theme development
* [BEM](http://getbem.com/introduction/) methodology to help with CSS class naming and creating reusable components.
* [Stave](https://github.com/barrel/svbstrate), a light functional CSS library to supplement BEM definitions.
* [GitLab CI](https://docs.gitlab.com/ee/ci/) config for testing 
* [Netlify](https://netlify.com/) hosting for static website

[Back to Top](#table-of-contents)

### Requirements

- [Node.js](https://nodejs.org/en/download/)

  At the time of this writing, we use the stable node `v9.11.1` and npm `v6.5.0` packages.

- [Lando](https://github.com/lando/lando/releases)

  We use **Lando** to standardize the environment between developer workstations. You can think of Lando as both an abstraction layer and superset of Docker.

  _The first rule of Lando club is do not update Docker. There can be problems if you do not use the version of Docker that ships with Lando. If you update Docker outside of this, YMMV._

[Back to Top](#table-of-contents)

### Initial Setup

After installing the main requirements, download the project's node dependencies:

```
npm ci
```

[Back to Top](#table-of-contents)

### Define Environment Variables

Once the project's node dependencies are installed, set up environment variables before running the build:

```
bash ./scripts/setup_env.sh
```

The `.env` should be created after running the script. Edit and add other protected strings to this file.

[Back to Top](#table-of-contents)

#### Build and Start Lando Server

Build the projects `dist/` output and start the server:

```
npm run serve
```

[Back to Top](#table-of-contents)

#### Build Project

If you want to simply rebuild the `dist/` directory:

```bash
npm run build
```

[Back to Top](#table-of-contents)

## Development and Project Features

After completing the steps above, you are ready for active development. The main command needed to generate the `dist/` output of static code is `npm run build`. 

For rapid development, use the features highlighted below.

[Back to Top](#table-of-contents)

### Browsersync

To leverage browsersync, watch files for hot module reload, and enable in-browser syntax linting, use the default `npm start` command. 

This will start a browsersync instance that proxies the lando server using the custom `browsersync-webpack.js` config bundle. 

[Back to Top](#table-of-contents)

### Environment Variables

The project's npm commands for `start` and `build` declare the `NODE_ENV` flag of either "development" or "production", respectively, which creates a boolean property `isDevMode` to enable several config option switches in `webpack.config.js`. Incidentally, a boolean variable `is_dev_mode` is also exported to enable this distinction in liquid templates as well. 

Protected variables like API keys and passwords should never be contributed to the project. This project uses [`dotenv`](https://github.com/motdotla/dotenv), to be able to define environment and protected variables and use them throughout the project.

When a new protected/environment variable is needed, simply modify the following files: 

- `scripts/setup_env.sh` - for future installs, should match the `.env` file
- `.env` - for use on any environment, ignored by git
- `webpack.config.js` - for use in scripts loaded via webpack, defined at the bottom, follow existing convention 
- `src/data/settings.js` - for liquid templates, can access the global `settings.*` variables.

[Back to Top](#table-of-contents)

### Other Scripts

There are a variety of additional build and test scripts provided in `package.json` that can be run directly in your terminal window. All scripts can be run via `npm run <scriptName>`. 

A suite of scripted tests are contained in `scripts/test_grammars.sh` and can be run with `npm test`.

[Back to Top](#table-of-contents)

### GitLab & GitLab CI

This project's source code is hosted on GitLab and uses GitLab CI, which will run `npm test` in a pipeline that preceeds deployment.

[Back to Top](#table-of-contents)

### Netlify

This project's static code is hosted on Netlify and uses a direct API integration for deployment, which can be monitored in GitLab's CI Pipelines. 

The master branch deploys to [production](https://base-modules.netlify.com/) and each feature branch has branch-based URLs while Merge Requests have preview-based URLs.

[Back to Top](#table-of-contents)

## Assemble & Shopify

This project uses [assemble](https://assemble.io) with the [liquidjs](https://github.com/harttle/liquidjs) templating engine to closely match and simplify Shopify theme development.

[Back to Top](#table-of-contents)

### Data

To establish global data available to any template, simply add a JSON file to the `src/data/` directory. The name of the file will become the global variable name, and the JSON contents will be assigned to the variable. 

Alternatively, add a property and value to the `global.js` data file in the same directory, and each key within the main export will be included as a global variable with the value as its assignment.

Data files should be treated much like a Theme Setting or Custom Field in Shopify.

[Back to Top](#table-of-contents)

### Modules

Modules are an abstraction of Shopify snippets. Housed in the `src/modules/$module-name/` directory are discreet, portable, and related liquid (HTML markup), CSS, Javascript (optionally Vue), and mock JSON files, which we collectively call a module. These files all share the same module name as its file name with respective file type endings.

Each module's CSS and JS files are loaded and bundled by webpack while the mock JSON is loaded by Assemble via a custom liquidjs plugin for the section tag. 

The sample mock data for `modules/text-focus/text-focus.json`:
```json
{
  "section": {
    "settings": {
      "title": "Hear From Our Customers",
      "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pharetra sodales porta. Phasellus iaculis nibh purus, id dictum ex"
    }
  }
}
```

Other custom liquid tags and filters that are not supported natively by liquidjs are found in the `liquid/` directory.

[Back to Top](#table-of-contents)

### Sections

Sections are the same as a Shopify section and are responsible for loading the module and its mock data that would typically be provided from Shopify section settings. 

In normal Shopify development these are stored in the `src/sections` directory, but in this case, section templates are housed with the respective module that it loads, uses the same name, yet the file ends in `.section.liquid`.

The section template should ideally load the module (snippet) with the `render` tag and include the `schema` tag for Shopify section settings. Since there is no way to replicate this in assemble, the schema file must be tested in Shopify. Schema will be discarded since the mock data will be used directly, but it's important to ensure a working schema for easy portability.

The sample section for `modules/text-focus/text-focus.section.liquid`:

```liquid
{%- render 'text-focus', item: section.settings -%}

{% schema %}
  {
    "name": "Text Focus",
    "settings": [{
      "type": "text",
      "id": "title",
      "label": "Title"
    }, {
      "type": "textarea",
      "id": "text",
      "label": "Text"
    }]
  }
{% endschema %}
```

[Back to Top](#table-of-contents)

### The Module API 
Any module that needs to leverage it's corresponding ES6 javascript file must simply declare the `data-module="$module-name"` attribute on the root of the module markup. 

The javascript module should export a function that accepts a single parameter, which is the the same root module element.

VueJS is also supported and loaded if needed by simplying using the `is="$module-name"` attribute, but it will look for a `$module-name.vue.js` instead.

[Back to Top](#table-of-contents)

### Project Structure
<pre>
/                             # → Root of Basic
├── .babelrc                  # → Babel Config
├── .editorconfig             # → EditorConfig
├── .eslintrc.js              # → EsLint/Standard Config
├── .gitignore                # → GitIgnore File Pattern
├── .gitlab-ci.yml            # → GitLab CI Config
├── .lando.yml                # → Lando Config
├── .liquidrc.js              # → Liquid Linter Config
├── .stylelintrc              # → StyleLint Config
├── assemblefile.js           # → Assemble Config
├── browsersync-webpack.js    # → Browsersync + Webpack config
├── CHANGELOG.md              # → Project Changelog
├── config.yml                # → Browsersync proxy targets
├── liquid/                   # → Liquid Plugins
│   ├── filters/*.js          # → Custom Liquid Filters
│   └── tags/*.js             # → Custom Liquid Tags
├── node_modules/             # → Node.js packages (never edit)
├── package-lock.json         # → Node.js dependency lockfile (new packages only)
├── package.json              # → Node.js dependencies and scripts
├── paths.js                  # → Custom paths to load liquid in assemble
├── postcss-tasks/            # → Custom PostCSS tasks/importers
├── postcss.config.js         # → PostCSS Config
├── README.md                 # → This file 
├── scripts/                  # → Project Bash Scripts
├── server/                   # → Lando Server Config Files
├── src/                      # → Theme source
│   ├── assets/               # → Static Assets
│   │   ├── images/           # → Image files
│   │   └── svg/              # → SVG files
│   ├── css/                  # → Global CSS files
│   ├── data/                 # → Global Data/JSON files
│   ├── js/                   # → Global JS files
│   ├── layout/               # → Layout templates
│   ├── locales/              # → Locale JSON files
│   ├── modules/              # → Module [Snippet] Templates
│   ├── pages/                # → Page Structure Templates
│   ├── sections/             # → Section Setting Templates
│   └── templates/            # → Templates
└── webpack.config.js         # → Webpack config
</pre>

[Back to Top](#table-of-contents)
