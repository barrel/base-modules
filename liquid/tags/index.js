module.exports = function (engine) {
  require('./form.js')(engine)
  require('./paginate.js')(engine)
  require('./schema.js')(engine)
  require('./section.js')(engine)
}
