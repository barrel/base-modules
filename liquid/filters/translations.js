const objectPath = require('object-path')
const locale = require('../../src/locales/en.default.json')

module.exports = function (engine) {
  engine.registerFilter('t', (key, value) => {
    try {
      let translation = objectPath.get(locale, key) || key;
      let data = {}
      if (typeof value !== 'undefined') {
        data[value[0]] = value[1]
      }
      return engine.parseAndRenderSync(translation, data)
    } catch (error) {
      return key
    }
  })
}
