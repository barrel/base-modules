require('dotenv').config()

module.exports = {
  'is_dev_mode': process.env.NODE_ENV !== 'production',
  'yotpo_app_key': process.env.YOTPO_APP_KEY
}
