const cart = require('./cartjs/data.json')
const productBase = require('./product.js')
const orderBase = require('./order.js')

const deepClone = obj => {
  return JSON.parse(JSON.stringify(obj))
}

const products = []

for (let i = 0; i < 50; i++) {
  const product = deepClone(productBase)
  product.id = `product-${i}`
  if (i === 0) {
    product.title = 'Lemtosh Dust'
    product.tags = [
      'material-dust'
    ]
  }
  products.push(product)
}

module.exports = {
  title: 'Barrel | Base Modules',
  shop: {
    name: 'Barrel Base Modules'
  },
  form: {
    'form.posted_successfully?': true
  },
  checkout: false,
  cart: cart,
  search: {
    results: [
      products[0],
      products[1],
      products[2]
    ]
  },
  collection: {
    handle: 'All',
    title: 'Default Collection',
    description: 'This is a description',
    url: 'collections/All',
    image: '/assets/PLP_Originals_Optical_1440x.jpg?v=1528847057',
    products: products
  },
  paginate: {
    pages: 1
  },
  current_page: 1,
  customer: {
    orders: [deepClone(orderBase)],
    default_address: {
      first_name: 'Barrel',
      last_name: 'O\'laughs',
      company: 'Barrel',
      address1: '197 Grand St',
      address2: 'Suite 7S',
      city: 'New York',
      province: 'NY',
      zip: 11249,
      country: 'United States',
      country_code: 'US',
      province_code: 'NY',
      phone: '123 456 7890'
    },
    addresses: [{
      first_name: 'Barrel',
      last_name: 'O\'laughs',
      company: 'Barrel',
      address1: '197 Grand St',
      address2: 'Suite 7S',
      city: 'New York',
      province: 'NY',
      zip: 11249,
      country: 'United States',
      country_code: 'US',
      province_code: 'NY',
      phone: '123 456 7890'
    }]
  },
  order: deepClone(orderBase),
  newsletter: {
    provider: 'klaviyo',
    mailchimp_url: 'https://drjart.us7.list-manage.com/subscribe/post-json?u=1c45fceca4996e6cafb8f60d2&amp;id=c646eeb648',
    klaviyo_url: 'https://manage.kmail-lists.com/ajax/subscriptions/subscribe',
    klaviyo_group: 'PP8Dfr',
    form_placeholder: 'Email',
    form_error_message: 'An error occurred. Please check the email address and try again.',
    form_success_message: 'You have been subscribed to the mailing list.',
    form_btn_text: 'Submit'
  },
  settings: {
    contacts_phone: '000',
    contacts_email: 'xx@xx.com',
    contacts_chat_url: '1234',
    contacts_phone_text: 'Phone',
    contacts_email_text: 'Email',
    contacts_chat_text: 'Chat',
    contacts_phone_image: 'icon-phone_669df080-b015-42ba-a6a2-ccab4ea174c3_200x.png?v=1529427195',
    contacts_email_image: 'icon-email_3df21641-2a8f-4225-9183-685a936180ae_200x.png?v=1529427202',
    contacts_chat_image: 'icon-chat_ee98c7fc-e2b3-4fdc-96ea-f589907baf76_200x.png?v=1529427210'
  }
}
