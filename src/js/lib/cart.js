import nanoajax from 'nanoajax'

class Cart {
  get () {
    return new Promise(resolve => {
      nanoajax.ajax({
        url: '/cart.js',
        method: 'get'
      }, (code, responseText, request) => {
        const cart = JSON.parse(responseText)
        resolve(cart)
      })
    })
  }

  generateShopifyCartUpdateBody ({ id, quantity }) {
    return `updates[${id}]=${quantity}`
  }

  update (data) {
    return new Promise((resolve, reject) => {
      nanoajax.ajax({
        url: '/cart/update.js',
        method: 'post',
        body: this.generateShopifyCartUpdateBody(data)
      }, (code, responseText, request) => {
        const cart = JSON.parse(responseText)
        if (Number(code) !== 200) {
          cart['errors'] = [cart.description]
        }
        resolve(cart)
      })
    })
  }

  /**
   * Serialize request body
   *
   * @param {Array} variants
   * @returns {string} HTTP Query
   */
  generateShopifyCartAddBody (variants) {
    return variants.reduce((query, variant, index) => {
      const {id, quantity, properties} = variant

      query += `&items[${index}][quantity]=${quantity}&items[${index}][id]=${id}`

      if (!properties || !Object.keys(properties).length) {
        return query
      }

      return Object.keys(properties).reduce((query, key) => {
        query += `&items[${index}][properties][${key}]=${properties[key]}`

        return query
      }, query)
    }, '')
  }

  /**
   * Adds one or multiple variants to the cart
   *
   * @param {Array} variants [{id: <id>, quantity: <Number>}]
   * @returns {Promise} Resolves with the line items object
   */
  add (variants) {
    return new Promise(resolve => {
      nanoajax.ajax({
        url: '/cart/add.js',
        method: 'post',
        body: this.generateShopifyCartAddBody(variants)
      }, (code, responseText, request) => {
        const cart = JSON.parse(responseText)
        resolve(cart)
      })
    })
  }
}

export default new Cart()
