import Vue from 'vue'
import state from 'lib/appState'

export default el => new Vue({
  el,
  data () {
    return {
      state
    }
  },
  methods: {
    toggleSearchOverlay () {
      this.state.isSearchOpen = !this.state.isSearchOpen
    }
  }
})
