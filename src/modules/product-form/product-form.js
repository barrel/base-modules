import Vue from 'vue'
import cart from 'lib/cart'
import state from 'lib/appState'

export default el => {
  return new Vue({
    el,
    data () {
      const product = window.BARREL.product
      const selected = product['options_with_values'].reduce((obj, option) => {
        obj[option.name] = option.values[0]
        return obj
      }, {})
      return {
        state,
        product,
        selected,
        quantity: 1
      }
    },
    watch: {
      selected: {
        handler () {
          this.pickActiveVariant()
        },
        deep: true
      }
    },
    mounted () {
      this.pickActiveVariant()
    },
    methods: {
      pickActiveVariant () {
        const {
          variants = [],
          options = []
        } = this.product

        for (let i = 0; i < variants.length; i++) {
          const variant = variants[i]
          const isMatch = options.every((option, _i) => {
            return (
              variant[`option${_i + 1}`] === this.selected[option]
            )
          })
          if (isMatch) {
            state.activeVariant = variant
            break
          }
        }
      },
      updateSelectedOption (name, value) {
        Vue.set(this.selected, name, value)
      },
      addToCart () {
        cart.add({
          id: this.state.activeVariant.id,
          quantity: this.quantity
        }).then(data => {
          state.isMiniCartOpen = true
        })
      }
    }
  })
}
