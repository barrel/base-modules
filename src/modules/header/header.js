import on from 'dom-event'
import select from 'select-dom'
import {
  set,
  unset,
  toggle,
  contains,
  getHeight,
  isOver
} from 'lib/util'

// variables
const off = on.off
const breakpoint = 999

// 'close' function - remove is-active and set height to 0.
const close = (links, groups) => {
  //
  unset(groups.concat(links), 'is-active')
  for (let group of groups) group.style.height = '0px'
}

class Header {
  constructor (el) {
    this.el = el
    this.links = select.all('.js-link', el)
    this.groups = select.all('.js-subnav', el)
    this.btnMobile = select('.js-btn-mobile', el)
    this.nav = select('.js-nav', el)
    this.queuedAction = false

    this.hideNavigation = this.hideNavigation.bind(this)
    this.toggleSubNavByClick = this.toggleSubNavByClick.bind(this)
    this.toggleMobileNav = this.toggleMobileNav.bind(this)
    this.resizeHandler = this.resizeHandler.bind(this)
    this.hideActiveMenu = this.hideActiveMenu.bind(this)
    this.openSubNavOnEnter = this.openSubNavOnEnter.bind(this)
    this.closeSubNavOnLeave = this.closeSubNavOnLeave.bind(this)
    this.cancelClose = this.cancelClose.bind(this)

    this.attachEventHandlers()
    this.resizeHandler()
  }

  attachEventHandlers () {
    for (let link of this.links) {
      on(link, 'click', this.toggleSubNavByClick)
      on(link, 'mouseenter', this.openSubNavOnEnter)
      on(link, 'mouseleave', this.closeSubNavOnLeave)
    }

    for (let group of this.groups) {
      on(group, 'mouseleave', this.closeSubNavOnLeave)
    }
    on(this.btnMobile, 'click', this.toggleMobileNav)
    on(window, 'resize', this.resizeHandler)
    on(window, 'scroll', this.scrollHandler)
    on(window, 'click', this.hideActiveMenu)
  }

  toggleSubNavByClick (e) {
    const el = e.target
    const link = e.currentTarget
    const isActive = contains(link, 'is-active')
    const group = select('.js-subnav', link)

    if (isOver(breakpoint)) {
      return
    }

    close(this.links, this.groups)

    if (!el.getAttribute('href')) {
      e.preventDefault()
      e.stopPropagation()
    }

    if (!isActive) {
      group.style.height = getHeight(group.children[0])
      set([group, link], 'is-active')
    }
  }

  openSubNavOnEnter (e) {
    if (!isOver(breakpoint)) {
      return
    }

    const link = e.currentTarget
    const isActive = contains(link, 'is-active')
    const group = select('.js-subnav', link)

    if (!isActive) {
      group.style.height = getHeight(group.children[0])
      set([group, link], 'is-active')
    }
  }

  closeSubNavOnLeave (e) {
    if (!isOver(breakpoint)) {
      return
    }

    const link = e.currentTarget
    const isActive = contains(link, 'is-active')

    if (!isActive) {
      return
    }

    this.queuedAction = setTimeout(() => {
      close(this.links, this.groups)
    }, 300)
  }

  cancelClose () {
    clearTimeout(this.queuedAction)
  }

  toggleMobileNav () {
    toggle(this.btnMobile, 'is-active')
    toggle(this.nav, 'is-active')

    if (contains(this.nav, 'is-active')) {
      set(this.nav, 'is-animated')
    } else {
      setTimeout(() => {
        unset(this.nav, 'is-animated')
      }, 300)
    }
    this.nav.style.height = (contains(this.nav, 'is-active')) ? `calc(100vh - ${getHeight(this.el)})` : '0px'
  }

  hideNavigation () {
    close(this.links, this.groups)
    unset(this.btnMobile, 'is-active')
    unset(this.nav, 'is-active')
    this.nav.style.height = '0px'
  }

  resizeHandler () {
    if (isOver(breakpoint)) {
      /* Close mobile nav if active */
      if (contains(this.nav, 'is-active')) {
        this.hideNavigation()
      }
    }
    /* close navigation on desktop */
    close(this.links, this.groups)
  }

  hideActiveMenu (e) {
    close(this.links, this.groups)
  }

  destroy () {
    off(this.header, 'click', this.toggleSubNav)
    off(this.btnMobile, 'click', this.toggleMobileNav)
    off(window, 'resize', this.resizeHandler)
    off(window, 'scroll', this.scrollHandler)
    off(window, 'click', this.hideActiveMenu)
  }
}

export default el => new Header(el)
