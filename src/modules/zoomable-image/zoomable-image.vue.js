import Vue from 'vue'

Vue.component('zoomable-image', resolve => {
  require(['vue-zoom'], vZoom => resolve({
    props: {
      src: String
    },
    components: {
      vZoom
    },
    updated (next) {
      // We need to fade in the images here
      // when the user changes the active image src
      // to make the transition less aggressive
    },
    template: `
    <div :key="src">
      <v-zoom :img="src" class="zoomImg" width="100%" />
    </div>
    `
  }))
})
