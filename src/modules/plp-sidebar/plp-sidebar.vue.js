import Vue from 'vue'
import {mapState, mapActions} from 'vuex'
import {
  selectedFacetsIndex
} from 'modules/plp/@util'
import 'modules/color-circle/color-circle.vue.js'
import 'modules/checkbox/checkbox.vue.js'
import 'modules/tooltip/tooltip.vue.js'

Vue.component('plp-sidebar', {
  props: [
    'title'
  ],
  data () {
    return {
      collectionListActive: false
    }
  },
  computed: {
    ...mapState([
      'facets',
      'selectedFacets',
      'showFilterMenu',
      'showCollectionMenu'
    ])
  },
  methods: {
    ...mapActions([
      'updateSelectedFacets',
      'toggleFilterMenu',
      'resetSelectedFacets',
      'toggleCollectionMenu'
    ]),
    isActiveFilter (group, facet) {
      return ~selectedFacetsIndex(this.selectedFacets, {
        name: group,
        value: facet
      })
    }
  },
  template: `
  <div :class="{'is-collection-list-active': showCollectionMenu}">
    <div
      class="plp-sidebar__show-collections"
      @click="toggleCollectionMenu(!showCollectionMenu)">
      <a
        href="javascript:void(0)"
        class="h5">Category</a>
      <i class="icon block">
        <svg viewBox="0 0 13 8">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-down" x="0" y="0"></use>
        </svg>
      </i>
    </div>
    <slot></slot>
    <ul
      class="plp-sidebar__list plp-sidebar__list--filters"
      :class="{'is-active': showFilterMenu}">
      <div class="plp-sidebar__apply-filter">
        <a
          href="javascript:void(0)"
          @click="toggleFilterMenu(false)"
          class="button block align-c">Apply Filters</a>
      </div>
      <li
        v-for="key in Object.keys(facets)"
        :key="'filter-group' + key"
        class="plp-sidebar__list-group plp-sidebar__list-group--shape">
        <h3 class="h5">{{key}}</h3>
        <ul>
          <li
            class="plp-sidebar__list-group-item plp-sidebar__list-group-item--filter"
            v-for="(facet, i) in facets[key]"
            @click="updateSelectedFacets({name: key, value: facet})">
            <checkbox
              :key="facet + isActiveFilter(key, facet)"
              :checked="isActiveFilter(key, facet)" />
            <span class="p2 uppercase">{{facet}}</span>
          </li>
        </ul>
      </li>
      <div class="plp-sidebar__reset-filter align-c">
        <a
          href="javascript:void(0)"
          v-on:click="resetSelectedFacets"
          class=underline>Reset Filters</a>
      </div>
    </ul>
    <transition name="fade">
      <div
        v-if="showFilterMenu"
        v-on:click="toggleFilterMenu(false)"
        class="plp-sidebar__list-overlay"></div>
    </transition>
  </div>`
})
