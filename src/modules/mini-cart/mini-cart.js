import Vue from 'vue'
import store from 'modules/cart-grid/@store'

export default el => new Vue({el, store})
