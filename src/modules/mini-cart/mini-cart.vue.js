import Vue from 'vue'
import { mapState, mapMutations } from 'vuex'

Vue.component('mini-cart', {
  computed: {
    ...mapState(['app']),
    cart () {
      return this.app.cart
    }
  },
  methods: {
    ...mapMutations(['toggleMiniCart'])
  }
})
