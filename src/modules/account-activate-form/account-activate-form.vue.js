import Vue from 'vue'
import validate from 'mixins/validate'

Vue.component('account-reset-form', {
  data () {
    return {}
  },
  mixins: [validate]
})
