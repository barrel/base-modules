import Vue from 'vue'
import state from 'lib/appState'

export default el => new Vue({
  el,
  data () {
    return {
      state
    }
  },
  methods: {
    toggleMiniCart () {
      this.state.isMiniCartOpen = !this.state.isMiniCartOpen
    }
  },
  computed: {
    itemCount () {
      return (this.state.cart.items || [])
        .reduce((count, item) => {
          count += Number(item.quantity)
          return count
        }, 0)
    },
    itemCountText () {
      return `Your bag - ${this.itemCount} items`
    }
  }
})
