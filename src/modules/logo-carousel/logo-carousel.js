import Flickity from 'flickity'
import select from 'select-dom'
import on from 'dom-event'
import {
  getWidth
} from 'lib/util'

const getTotalWidth = (items) => {
  let breakpoint = 0

  for (let item of items) {
    breakpoint += item.offsetWidth
  }

  return breakpoint
}

export default (el) => {
  let carousel = select('.js-carousel', el)
  let slides = select.all('.js-slide', el)

  let flick = new Flickity(carousel, {
    cellAlign: 'center',
    contain: true,
    draggable: false,
    pageDots: false,
    prevNextButtons: false,
    on: {
      ready: function () {
        this.options.draggable = (getTotalWidth(slides) > carousel.offsetWidth)
        this.updateDraggable()
      }
    }
  })

  on(window, 'resize', () => {
    flick.options.draggable = (getTotalWidth(slides) > carousel.offsetWidth)
    flick.updateDraggable()
  })
}
