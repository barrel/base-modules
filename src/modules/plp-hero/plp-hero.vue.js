import Vue from 'vue'

Vue.component('plp-hero', {
  props: [
    'image',
    'title',
    'description'
  ]
})
