import Vue from 'vue'
import { mapState, mapMutations, mapGetters } from 'vuex'
import {disableBodyScroll, clearAllBodyScrollLocks} from 'body-scroll-lock'

import 'modules/cart-item/cart-item.vue'
import 'modules/quantity-selector/quantity-selector.vue'
import 'modules/image/image.vue'
import 'modules/shipping-calculator/shipping-calculator.vue'

Vue.component('cart-grid', {
  computed: {
    ...mapGetters(['subtotal']),
    ...mapState([
      'app',
      'loading',
      'isBootstrapped',
      'isMobile'
    ]),
    isEmpty () {
      return this.cart.items.length === 0
    },
    cart () {
      return this.app.cart
    }
  },
  watch: {
    'app.isMiniCartOpen' (value) {
      if (value) {
        disableBodyScroll(this.$refs['cart'])
      } else {
        clearAllBodyScrollLocks()
      }
    }
  },
  methods: {
    ...mapMutations(['bootstrap']),
    ...mapMutations(['toggleMiniCart'])
  },
  mounted () {
    this.bootstrap()
  }
})
