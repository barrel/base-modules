import Vue from 'vue'
import store from './@store'

export default el => new Vue({el, store})
