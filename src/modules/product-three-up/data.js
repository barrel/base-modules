const deepClone = obj => {
  return JSON.parse(JSON.stringify(obj))
}

const product1 = deepClone(require('../../data/product'))
const product2 = deepClone(require('../../data/product'))
const product3 = deepClone(require('../../data/product'))
const product4 = deepClone(require('../../data/product'))

product1.id = 11111111
product2.id = 11111112
product3.id = 11111113
product4.id = 11111114

module.exports = {
  'all_products': {
    lemtosh: product1,
    zev: product2,
    miltzen: product3,
    nebb: product4
  }
}
