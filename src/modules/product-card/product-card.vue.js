import Vue from 'vue'

import {
  getProductImage,
  decode
} from 'lib/util'

/**
 * This component is used throughout the site,
 * in product carousels and on the PLP page.
 */
Vue.component('product-card', {
  props: [
    'product',
    'decode',
    'featured'
  ],

  /**
   * Data can come via a JSON string that needs to be
   * decoded, or via an javascript object, passed by a
   * parent component or a Vuex store.
   */
  data () {
    let product
    if (!!this.decode && this.product) {
      product = JSON.parse(decode(this.product))
    } else {
      product = (this.product || {
        title: '',
        price: '',
        featuredImage: '',
        variants: [],
        optionNames: []
      })
    }

    return {
      ...product,
      activeOption: ''
    }
  },

  mounted () {
    this.activeOption = this.firstOption
  },

  computed: {
    firstOption () {
      return (this.options || [])[0]
    },
    optionName () {
      return (this.optionNames || [])[0]
    },
    options () {
      return this[this.optionName]
    },
    imageSrc () {
      return getProductImage(this.optionName, this.activeOption, this.images, false, this.featuredImage)
    },
    link () {
      return `/products/${this.handle}${this.activeVariantUrl}`
    },
    activeVariantUrl () {
      const firstVariant = this.variants.find(({title = '', id}) => {
        return ~title.indexOf(this.activeOption)
      })
      return (firstVariant ? `?variant=${firstVariant.id}` : '')
    }
  },

  methods: {
    changeActiveOption (value) {
      this.activeOption = value
    }
  }
})
