import Vue from 'vue'
import { updateSource } from './video.js'

Vue.component('vue-video', {
  props: ['source', 'poster'],
  data () {
    return {
      isLoaded: false
    }
  },
  mounted () {
    const offset = parseInt(this.$el.dataset.offset)
    const config = { offset }

    updateSource(this.$refs.source, this.$refs.video, config)
  },
  methods: {
    onCanPlay () {
      this.isLoaded = true
    }
  }
})
