import Flickity from 'flickity'
import select from 'select-dom'
import on from 'dom-event'
import {
  set,
  getHeight
} from 'lib/util'

const reset = (el) => {
  let slides = select.all('.js-slide', el)

  for (let slide of slides) {
    slide.style.height = null
  }
}

const resize = (el) => {
  let viewport = select('.flickity-viewport', el)
  let slides = select.all('.js-slide', el)
  let height = 0

  for (let slide of slides) {
    slide.style.height = getHeight(viewport)
  }
}

export default (el) => {
  let carousel = select('.js-carousel', el)
  let slides = select.all('.js-slide', el)

  let flick = new Flickity(carousel, {
    pageDots: true,
    prevNextButtons: false,
    on: {
      ready: () => {
        resize(el)
      }
    }
  })

  window.onload = () => {
    reset(el)
    flick.resize()
    resize(el)
  }

  on(window, 'resize', () => {
    reset(el)
    flick.resize()
    resize(el)
  })
}
