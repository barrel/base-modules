import Vue from 'vue'

Vue.component('plp-select', {
  props: [
    'name',
    'value'
  ],
  data: function () {
    return {}
  },
  computed: {
    inputValue () {
      return this.sort
    }
  },
  methods: {
    updateSort (e) {
      this.$store
        .dispatch('updateSort', e.target.value)
        .then(() => {
          this.$store.dispatch('fetchCollection', {
            ajaxing: true,
            initial: false,
            resetSelectedFacets: false
          })
        })
    }
  },
  template: `
  <div class="plp-select">
    <span class="plp-select__pre p2">Sort By:</span>
    <select class="plp-select__el p2" :name="name" :value="value" v-on:change="updateSort">
      <slot></slot>
    </select>
    <i class="plp-select__icon">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33.8 13.5">
        <path fill="none" stroke="#000" stroke-width="2" d="M33.2.8L16.9 12.3.6.8"></path>
      </svg>
    </i>
  </div>`
})
