import Vue from 'vue'
import validate from 'mixins/validate'

Vue.component('account-new-address', {
  props: [
    'firstName',
    'lastName',
    'company',
    'address1',
    'address2',
    'city',
    'province',
    'zip',
    'country',
    'phone',
    'isDefault',
    'id',
    'toggleNewAddressForm',
    'countries'
  ],
  data () {
    return {
      'activeCountry': this.getCountryFromCode(this.country)
    }
  },
  mixins: [validate],
  computed: {
    showProvinceSelect () {
      return (
        this.activeCountry &&
        this.activeCountry.zones.length
      )
    },
    formAction () {
      if (this.id) {
        return `/account/addresses/${this.id}`
      } else {
        return '/account/addresses'
      }
    }
  },
  methods: {
    getInitialValue (name) {
      const match = name.match(/\[(.*)\]/)
      if (!match[1]) {
        return ''
      }
      const str = match[1]

      switch (str) {
        case 'first_name':
          return this['firstName'] || ''
        case 'last_name':
          return this['lastName'] || ''
        case 'default':
          return this['isDefault'] || false
        default:
          return this[str] || ''
      }
    },
    getCountryFromCode (value) {
      return this.countries.find(country => country.code === value)
    },
    onInputChange ({name, value}) {
      const match = /\D\[(\D+)\]/.exec(name)
      if (!match || !match[1]) {
        return false
      }
      if (match[1] === 'country' && value) {
        this.activeCountry = this.getCountryFromCode(value)

        const country = this.countries[this.activeCountry]
      }
    },
    generateCountryArray () {
      return this.countries.map(country => ({
        label: country.name,
        value: country.code
      }))
    },
    generateProvinceArray () {
      return this.activeCountry.zones.map(zone => ({
        label: zone.name,
        value: zone.code
      }))
    }
  }
})
