import Vue from 'vue'
import VueScrollTo from 'vue-scrollto'
import 'unfetch/polyfill'
import 'es6-promise/auto'
import { loadCountries } from './@country-service'

Vue.use(VueScrollTo)

export default el => {
  return new Vue({
    el,
    data () {
      return {
        activeAddress: {},
        showNewAddressForm: false,
        countries: []
      }
    },
    created () {
      loadCountries('EN')
        .then(countries => {
          this.countries = countries
        })
    },
    methods: {
      toggleNewAddressForm (show) {
        this.showNewAddressForm = show
        show && this.$nextTick(() => {
          this.$scrollTo(this.$refs['newAddressForm'])
        })
      },
      populateAddressForm (data) {
        this.activeAddress = data
        this.toggleNewAddressForm(true)
      },
      showEmptyForm () {
        for (let key in this.activeAddress) {
          this['activeAddress'][key] = ''
        }
        this.toggleNewAddressForm(true)
      }
    }
  })
}
