import Vue from 'vue'
import validate from 'mixins/validate'

Vue.component('account-register-form', {
  data () {
    return {}
  },
  mixins: [validate]
})
