import Vue from 'vue'
import select from 'select-dom'
import on from 'dom-event'
import {
  set,
  map
} from 'lib/util'

export default el => new Vue({
  el,
  mounted () {
    this.$nextTick(() => {
      const pieces = select.all('.js-coming-soon-piece', this.$el)
      on(window, 'load', () => {
        set(this.$el, 'is-loaded')
        setTimeout(() => {
          map(pieces, (piece, i) => {
            setTimeout(() => set(piece, 'is-active'), i * 400)
          })
        }, 600)
      })
    })
  }
})
