const path = require('path')
const fs = require('fs')

const resolver = function(app) {
  // all paths relative to this file!!
  const moduleDir = './src/modules/'
  const rootPaths = [
    path.resolve(__dirname, './src/layout'),
    path.resolve(__dirname, './src/templates')
  ]

  fs.readdir(moduleDir, (err, files) => {
    files.forEach(fileName => {
      // if module-name.json exists
      let moduleData = `${moduleDir}${fileName}/${fileName}.json`
      if (fs.existsSync(moduleData)) {
        let sectionFile = `${moduleDir}${fileName}/${fileName}.section.liquid`
        if (!fs.existsSync(sectionFile)) {
          throw Error(`Missing section setting liquid file: ${sectionFile}
            Perhaps you should use a data file instead?`)
        }
      }

      // if data.js exists
      let dataFile = `${moduleDir}${fileName}/data.js`
      if (fs.existsSync(dataFile)) {
        app.data(require(path.resolve(__dirname, dataFile)))
      }
      rootPaths.push(path.resolve(__dirname, `${moduleDir}${fileName}`))
    });
  });

  return rootPaths
}

module.exports = resolver;
