# Change Log
All notable changes to this project will be documented in this file.

## 2.1.1 - 2019-11-13
### CHANGED:
- Update lando config

## 2.1.0 - 2019-11-12
### CHANGED:
- Migrate existing modules
- Migrate to new assemble build workflow

## 2.0.0 - 2019-11-01
### CHANGED:
- Initial commit for final resting place of original base modules 
